{ config, lib, pkgs, ... }:
with lib;
let
  cfg = config.elemental.home.service.media.mpd;
in
{
  options.elemental.home.service.media.mpd = {
    enable = mkEnableOption "Enable mpd for local music playing";
  };

  config = mkIf cfg.enable {
    services.mpd = {
      enable = true;
      musicDirectory = "${config.home.homeDirectory}/Music";
      playlistDirectory = "${config.home.homeDirectory}/Music/mpd-playlists";
      dataDir = "${config.xdg.dataHome}/mpd";
      extraConfig = ''
        audio_output {
          type    "pulse"
          name    "pulse audio"
        }
      '';
    };
  };
}
